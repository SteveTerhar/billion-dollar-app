package main

import (
  "fmt"
  "net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Billion Dollar App!!!!")
}

func setupRoutes() {
  http.HandleFunc("/", homePage)
}

func main() {
  fmt.Println("Running...")
  setupRoutes()
  http.ListenAndServe(":3000", nil)
}